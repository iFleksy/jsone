/*
    Task One 1.
    Дан код::
        var a = 1, b = 1, c, d;
        c = ++a; console.log(c);           // 2
        d = b++; console.log(d);           // 1
        c = (2+ ++a); console.log(c);      // 5
        d = (2+ b++); console.log(d);      // 4
        console.log(a);                    // 3
        console.log(b);                    // 3
        Почему код даёт именно такие результаты?
 */
function printTitle(taskNumber){
    console.log("********** Task: "+ taskNumber +" ***************");
}
printTitle(1);
var a = 1, b = 1, c, d;

// Сначала происходит инкремент после чего присваивание переменной
c = ++a; console.log("c = ++a; Out: " + c);

// Сначало происходит присваивание после чего инкремент
d = b++; console.log("d = b++; Out: " + d);

// a уже = 2. После чего происходит инкремент a++, a = 3. сложение 2 + 3 = 5.
c = (2 + ++a); console.log("c = (2+ ++a); Out: " + c);

// b уже = 2. После чего происходит сложение и присваивание к переменной d, а после инкремент b
d = (2+ b++); console.log("d = (2+ b++); Out: " + d);

// a после всех операций ++ стало 3
console.log("Out a: " + a);

// a после всех операций ++ стало 4
console.log("Out b: " + b);

/*
    Task 2.
    Чему будет равен x в примере ниже?
        var a = 2;
        var x = 1 + (a *= 2);
 */
printTitle(2);
var a = 2;
// a = a * 2
// a + 1
var x = 1 + (a *= 2);

console.log("var x = 1 + (a *= 2); Out: " + x);

/*
    Task 3.
    Объявить две целочисленные переменные a и b и задать им произвольные начальные значения.
    Затем написать скрипт, который работает по следующему принципу:
        1. если a и b положительные, вывести их разность
        2. если а и b отрицательные, вывести их произведение
        3. если а и b разных знаков, вывести их сумму
     ноль можно считать положительным числом
 */
printTitle(3)

function doneTask3(one, two){
    switch (true) {
        case one >= 0 && two >= 0:
            result = one - two;
            console.log("Разность " + one + " и " + two + " = " + result);
            break;
        case one < 0 && two < 0:
            result = one * two;
            console.log("Произведение " + one + " и " + two + " = " + result);
            break;
        default:
            result = one + two;
            console.log("Сумма " + one + " и " + two + " = " + result);
    }
}

doneTask3(4, 5);
doneTask3(-4, 10);
doneTask3(-4, -5);

/*
    Task 4.
    Присвоить переменной а значение в промежутке [0..15].
    С помощью оператора switch организовать вывод чисел от a до 15
 */
printTitle(4);
function doneTask4(number) {
    switch (true) {
        case number <= 1:
            console.log(1);
        case number <= 2:
            console.log(2);
        case number <= 3:
            console.log(3);
        case number <= 4:
            console.log(4);
        case number <= 5:
            console.log(5);
        case number <= 6:
            console.log(6);
        case number <= 7:
            console.log(7);
        case number <= 8:
            console.log(8);
        case number <= 9:
            console.log(9);
        case number <= 10:
            console.log(10);
        case number <= 11:
            console.log(11);
        case number <= 12:
            console.log(12);
        case number <= 13:
            console.log(13);
        case number <= 14:
            console.log(14);
        case number <= 15:
            console.log(15);
            break;
        default:
            console.log('Bad range');
    }
}
doneTask4(10);


/*
    Task 5.
    Реализовать основные 4 арифметические операции в виде функций с двумя параметрами.
    Обязательно использовать оператор return.
 */

function summa(one, two){
    return one + two;
}

function difference(one, two){
    return one - two;
}

function division(one, two){
    return one / two
}

function multiplication(one, two) {
    return one * two;
}

/*
    Task 6.
    Реализовать функцию с тремя параметрами:
    function mathOperation(arg1, arg2, operation),
    где arg1, arg2 – значения аргументов, operation – строка с названием операции.
    В зависимости от переданного значения операции выполнить одну из арифметических операций
    (использовать функции из пункта 3) и вернуть полученное значение (использовать switch).
 */
printTitle(6);
function doIt(one, two, operation){
    switch(operation) {
        case 'summa':
            return summa(one, two);
        case 'difference':
            return difference(one, two);
        case 'division':
            return division(one, two);
            break;
        case 'multiplication':
            return multiplication(one, two);
            break;
        default:
            console.log("Bad operation")
    }
}
console.log("Values: 100 and 10");
var sum = doIt(100, 10, 'summa');
console.log("Сумма: " + sum);
sum = doIt(100, 10, 'difference');
console.log("Разница: " + sum);
sum = doIt(100, 10, 'division');
console.log("Деление: " + sum);
sum = doIt(100, 10, 'multiplication');
console.log("Умножение: " + sum);

/*
    Task 7.
     Сравнить null и 0. Попробуйте объяснить результат
 */
printTitle(7);
var res;
res = 0 == null;
console.log("0 == null; Out: " + res);

res = 0 > null;
console.log("0 > null; Out: " + res);

res = 0 < null;
console.log("0 < null; Out: " + res);

res = 0 >= null;
console.log("0 >= null; Out: " + res);

res = 0 <= null;
console.log("0 <= null; Out: " + res);

res = 0 === null;
console.log("0 === null; Out: " + res);

// null неопределённое значение. из за этого такой результат

/*
    Task 8.
    С помощью рекурсии организовать функцию возведения числа в степень.
    Формат: function power(val, pow), где val – заданное число, pow – степень.
 */
printTitle(8);

function power(val, pow) {
    return pow > 1 ? val * power(val, pow - 1) : val;
}
console.log("3^3");
console.log(power(3, 3));
