function printTitle(taskNumber){
    console.log("********** Task: "+ taskNumber +" ***************");
}

/*
    Task 1.
     С помощью цикла while вывести все простые числа в промежутке от 0 до 100.
 */
printTitle(1);

var number = 0;
while (number <= 100) {
    isPrime = true;
    for (var i = 2; i < number; i++){
        if ((number % i) == 0) {
            isPrime = false;
            break;
        }
    }
    if (isPrime) {
        console.log(number);
    }
    number++;
}

/*
    Task 2.
    С этого урока начинаем работать с функционалом интернет-магазина.
    Предположим, есть сущность корзины. Нужно реализовать функционал подсчета стоимости
    корзины в зависимости от находящихся в ней товаров.

    Товары в корзине хранятся в массиве. Задачи:
        a) Организовать такой массив для хранения товаров в корзине;
        b) Организовать функцию countBasketPrice, которая будет считать стоимость корзины.
 */
printTitle(2);
printTitle(3);
var items = [
    {
        name: "Mobile",
        price: 5000,
    },
    {
        name: "Avatar",
        price: 1500,
    },
    {
        name: "OneTouch",
        price: 600,
    }
];

function countBasketPrice(items){
    let price = 0;
    items.forEach(function (value, index) {
        price += value.price;
    });
    return price
}
console.log(countBasketPrice(items));

/*
    Task 4
    Вывести с помощью цикла for числа от 0 до 9, не используя тело цикла. Выглядеть это должно так:
    for(…){// здесь пусто}
 */
printTitle(4);
for(var i = 0; i <= 9; console.log(i++)){}

/*
    Task 5
    5. *Нарисовать пирамиду с помощью console.log,
    как показано на рисунке, только у вашей пирамиды должно быть 20 рядов, а не 5:
    x
    xx
    xxx
    xxxx
    xxxxx
 */
printTitle(5);
function printPiromid(rows){
    for (var i = 1; i <= rows; i++){
        row = "";
        for (var x = 1; x <= i; x++){
            row += "*"
        }
        console.log(row);
    }
}

printPiromid(20);
